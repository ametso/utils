/* Disposable Password Generator Utility.
 *
 * Copyright (c) 2012 Ari Metso
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>

#define MINLEN 8
#define MAXLEN 12
#define DIGIT_RND_MAX (10)
#define ALPHA_RND_MAX (26)

int str_to_int(char s[]) {
    int i, n;

    n = 0;
    for (i = 0; (n < 1000 && isdigit(s[i])); i++) {
        n = 10 * n + (s[i] - '0');
    }
    return n;
}

int main(int argc, char **argv) {
    int passlen = MINLEN;
    if (argc > 1) {
        passlen = str_to_int(argv[1]);
        if (passlen < MINLEN || passlen > MAXLEN) {
            printf("Password length %d invalid. Must be in [8..12], default is 8.\n", passlen);
            return 1;
        }
    }

    /* create passwd string here */
    char passwd[MAXLEN + 1];

    /* initialize random generator */
    srand((int)time(NULL));

    /* first 2 chars are digits, next 5 - 9 lower-case letters, the last is upper-case letter */
    int i = 0;
    while (i < passlen) {
        int rnd_max = i < 2 ? DIGIT_RND_MAX : ALPHA_RND_MAX;
        int rndvalue = rand() % rnd_max;

        if (i < 2) {
            passwd[i] = '0' + rndvalue;
        } else if (i < passlen - 1) {
            passwd[i] = 'a' + rndvalue;
        } else {
            passwd[i] = 'A' + rndvalue;
        }

        /* skip one and zero lookalikes */
        if (passwd[i] == 'l' || passwd[i] == 'I' || passwd[i] == 'O' || passwd[i] == '1' || passwd[i] == '0') {
            continue;
        }

        i++;
    }
    passwd[i] = '\0';
    printf("%s\n", passwd);

    return 0;
}
