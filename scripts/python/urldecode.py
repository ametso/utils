#!/bin/env python
#
# Urldecode strings in (somewhat of) unix style.
# Parameters are checked and decoded first.
# If no parameters are found read from standard input.

import fileinput
import urllib
import sys

if len(sys.argv) > 1:
    for arg in sys.argv[1:]:
        print urllib.unquote(arg)
else:
    for line in fileinput.input():
        print urllib.unquote(line.strip())

