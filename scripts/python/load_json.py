#!/usr/bin/env python
#
# Load a JSON response over HTTP and prettyprint it.

import json
import urllib2
import sys

if len(sys.argv) < 2:
    sys.exit('Usage: python {0} url'.format(sys.argv[0]))

url = sys.argv[1]
data = json.loads(urllib2.urlopen(url).read())
print json.dumps(data, indent=4)

