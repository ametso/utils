#!/usr/bin/env python
"""This script triggers a (Linux) desktop notification using dbus."""

import dbus
import getopt
import sys

ITEM = ('org.freedesktop.Notifications')
PATH = ('/org/freedesktop/Notifications')
INTERFACE = ('org.freedesktop.Notifications')

def exit_with_usage():
    sys.exit("Usage: python {} [-i path-to-icon-image] [-t display-time-in-seconds] [-a application-name] [-h notification-header] notification-text".format(sys.argv[0]))

array = ""
hint = ""
application_name = ""
notification_header = ""
notification_body = ""
notification_icon = ""
notification_time = 10000    # Use seconds x 1000
try:
    options, args = getopt.getopt(sys.argv[1:], "a:h:i:t:")
    for key, value in options:
        if key == "-a":
            application_name = (value);
        if key == "-h":
            notification_header = (value);
        if key == "-i":
            notification_icon = value;
        if key == "-t":
            notification_time = int(value) * 1000;
    notification_body = (args[0]);
except getopt.GetoptError, err:
    exit_with_usage()
except IndexError, err:
    exit_with_usage()
except ValueError, err:
    exit_with_usage()

icon = ''
array = ''
hint = ''
time = 10000   # Use seconds x 1000
app_name = ('Test Application')
title = ('NOTIFICATION TEST')
body = ('This is a test of the notification system via DBus.')

bus = dbus.SessionBus()
notif = bus.get_object(ITEM, PATH)
notify = dbus.Interface(notif, INTERFACE)
notify.Notify(application_name, 0, notification_icon, notification_header, notification_body, array, hint, notification_time)
